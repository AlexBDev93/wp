<?php
    session_start();

    require 'bootstrap.php';

    function show_session_messages() {
        if (isset($_SESSION['messages']) && !empty($_SESSION['messages']) && is_array($_SESSION['messages'])) {
            foreach ($_SESSION['messages'] as $message) {
                if (!empty($message)) {
                    echo '<span>'.$message.'</span></br>';
                }
            }

            unset($_SESSION['messages']);
        }
    }
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>WP Installer</title>
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    </head>
    <body>
        <h1>WP Installer</h1>
        <div>
            <?php show_session_messages(); ?>
        </div>

        <?php
            if (!wp_cli_exists()) {
                echo '<a href="?install_command">Unable to find the command '.WP_CLI.' click  to install</a>';
            }
        ?>
        <form method="POST">
            <label>Base path</label>
            <input type="text" name="wp_installer[base_path]" value="/var/www/html/">
            <br>
            <label>Directory name</label>
            <input type="text" name="wp_installer[directory_name]" required>
            <br>
            <label>Database hostname</label>
            <input type="text" name="wp_installer[database][hostname]" value="localhost" required>
            <br>
            <label>Database username</label>
            <input type="text" name="wp_installer[database][username]" value="root" required>
            <br>
            <label>Database password</label>
            <input type="password" name="wp_installer[database][password]" required>
            <br>
            <label>Database name</label>
            <input type="text" name="wp_installer[database][name]" required>
            <br>
            <label>locale</label>
            <input type="text" name="wp_installer[locale]" value="en_US" required>
            <br>
            <label>App url</label>
            <input type="text" name="wp_installer[app][url]" required>
            <br>
            <label>App title</label>
            <input type="text" name="wp_installer[app][title]" required>
            <br>
            <label>App admin user</label>
            <input type="text" name="wp_installer[app][admin_user]" required>
            <br>
            <label>App admin password</label>
            <input type="text" name="wp_installer[app][admin_password]" required>
            <br>
            <label>App admin email</label>
            <input type="text" name="wp_installer[app][admin_email]">
            <br>
            <label>[OPT] nginx configuration server name</label>
            <input type="text" name="wp_installer[server_conf][server_name]">
            <br>
            <select name="wp_installer[plugin][names][]" multiple>
                <option value="https://downloads.wordpress.org/plugin/wordfence.6.2.10.zip">Wordfence</option>
                <option value="duplicator-clone">Duplicator</option>
            </select>
            <input type="checkbox" name="wp_installer[plugin][options][0][active]">
            <input type="reset">
            <input type="submit">
        </form>
    </body>
</html>