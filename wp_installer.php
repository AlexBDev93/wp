<?php

require_once 'wp_installer_functions.php';

const BASE_PATH = '/var/www/html/';

if (isset($_POST['wp_installer'])) {

    $installer = $_POST['wp_installer'];
    $basePath = $installer['base_path'];

    if (empty($basePath)) {
        $basePath = BASE_PATH;
    }

    $fullpath = create_fullpath($basePath, $installer['directory_name']);
    $installer['fullpath'] = $fullpath;

    $database = $installer['database'];
    $database['locale'] = $installer['locale'];
    $database['fullpath'] = $fullpath;

    $app = $installer['app'];
    $app['fullpath'] = $fullpath;

    if (!isset($installer['plugin'])) {
        $installer['plugin'] = [];
    }

    $plugins = $installer['plugin'];
    $plugins['names'][] = 'duplicator';
    $plugins['names'][] = 'contact-form-7';
    $plugins['names'][] = 'wordfence';
    $plugins['names'][] = 'woocommerce';
    $plugins['names'][] = 'user-role-editor';
    $plugins['names'][] = 'stripe';
    $plugins['options']['path'] = $fullpath;
    $plugins['options'][] = 'activate';

//    if (isset($plugins['options']['active'])) {
//        unset($plugins['options']['active']);
//    }

    $messages = [];
    $logs = [];
    try {
        if (!wp_cli_exists()) {
            throw new \RuntimeException('Error : unable to find the command '.WP_CLI);
        }

        $logs[] = wp_init_project($installer);
        $logs[] = wp_config_database($database);
        $logs[] = wp_create_database($fullpath);
        $logs[] = wp_install_project($app);
//        $logs[] = wp_install_theme('theme name', $arg);

        $pluginLogs = wp_install_plugins($plugins['names'], $plugins['options']);
        $logs = array_merge($logs, $pluginLogs);


        if (!empty($installer['server_conf']['server_name'])) {
            $strConfig = createConfigServer('nginx.conf', [
                '%root_path%' => $fullpath,
                '%server_name%' => $installer['server_conf']['server_name'],
            ]);

            $messages[] = '<code>'.nl2br($strConfig).'</code>';
        }

    } catch (\Exception $e) {
        $logs[] = $e->getMessage();
    }

    $_SESSION['messages'] = array_merge($logs, $messages);
}

if (isset($_GET['install_command'])) {
    try {
        if (!wp_cli_exists()) {
            install_wp_cli();
            $_SESSION['messages'][] = 'Successfull installed';
        }
    } catch (\Exception $e) {
        $message = $e->getMessage();
        $_SESSION['messages'][] = $message;
    }
}
