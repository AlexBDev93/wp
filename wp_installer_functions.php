<?php
/**
 * Created by PhpStorm.
 * User: stagiaire
 * Date: 17/01/2017
 * Time: 09:25
 */

require_once 'wp_installer_config.php';
require_once 'wp_server_config_generator.php';

function create_fullpath($basePath, $directoryName) {
    if (substr($basePath, -1) !== '/') {
        $basePath .= '/';
    }

    $safeDirectoryName = trim($directoryName, '/');

    return $basePath.$safeDirectoryName;
}

function wp_cli_exists() {
    return !empty(exec('which '.WP_CLI));
}

function wp_init_project($parameters) {
    if (is_dir($parameters['fullpath'])) {
        throw new \InvalidArgumentException('Error : The directory '.$parameters['fullpath'].' already exists');
    }

    return wp_cli('core download', [
        'path' => $parameters['fullpath'],
        'locale' => $parameters['locale']
    ]);
}

function wp_config_database($parameters) {
    return wp_cli('core config', [
        'path' => $parameters['fullpath'],
        'dbname' => $parameters['name'],
        'dbuser' =>  $parameters['username'],
        'dbpass' => $parameters['password'],
        'locale' => $parameters['locale'],
    ]);
}

function wp_create_database($path) {
    return wp_cli('db create', [
        'path' => $path
    ]);
}

function wp_install_project($parameters) {
    return wp_cli('core install', [
        'path' => $parameters['fullpath'],
        'url' => $parameters['url'],
        'title' => '"'.$parameters['title'].'"',
        'admin_user' => $parameters['admin_user'],
        'admin_password' => $parameters['admin_password'],
        'admin_email' => $parameters['admin_email'],
        'skip-email'
    ]);
}

function wp_install_plugin($pluginName, $parameters) {
    $command = 'plugin install '.$pluginName;

    return wp_cli($command, $parameters);
}

function wp_install_theme($pluginName, $parameters) {
    $command = 'theme install --activate'.$pluginName;

    return wp_cli($command, $parameters);
}

function wp_install_plugins($pluginNames, $parameters) {
    $logs = [];
    foreach ($pluginNames as $pluginName) {
        $command = 'plugin install '.$pluginName;
        $logs[] = wp_cli($command, $parameters);
    }

    return $logs;
}

function wp_cli($command, $args) {
    $strArgs = transform_array_to_str_args($args);
    $strToExec = $command.' '.$strArgs;

    return exec(WP_CLI.' '.$strToExec);
}

function transform_array_to_str_args($parameters) {
    $args = array_map(function ($key, $value) {
        if (is_int($key)) {
            $str = '--'.$value;
        } else {
            $str = '--'.$key.'='.$value;
        }

        return $str;
    },
        array_keys($parameters),
        $parameters
    );

    $strArgs = implode(' ', $args);

    return $strArgs;
}

function install_wp_cli() {
    exec('curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar');
    exec('sudo chmod "u+x" /var/www/html/wp_installer/wp-cli.phar');
    exec('sudo mv /var/www/html/wp_installer/wp-cli.phar /usr/local/bin/'.WP_CLI);
}
