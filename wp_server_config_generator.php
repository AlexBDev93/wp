<?php
/**
 * Created by PhpStorm.
 * User: stagiaire
 * Date: 17/01/2017
 * Time: 09:42
 */

function getContentFileConfiguration($filename) {
    $filepath = getFilePathConfigurationToFile($filename);

    if (!file_exists($filepath)) {
        throw new \InvalidArgumentException(sprintf('Unable to load the configuration file in %s', $filepath));
    }

    return file_get_contents($filepath);
}

function getFilePathConfigurationToFile($filename) {
    return getAbsoluteFilePathConfiguration().$filename;
}

function getAbsoluteFilePathConfiguration() {
    return realpath('./config').'/';
}

function replaceFileConfigurationArgument($fileContent, $args) {
    return str_replace(array_keys($args), $args, $fileContent);
}

function createConfigServer($configFile, $parameters) {
    $fileContent = getContentFileConfiguration($configFile);

    return replaceFileConfigurationArgument($fileContent, [
        '%root_path%' => $parameters['fullpath'],
        '%server_name%' => $parameters['server_name'],
    ]);
}
